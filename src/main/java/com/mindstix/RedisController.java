/* ***************************************************************************
 * Copyright 2018 VMware, Inc.  All rights reserved.
 * -- VMware Confidential
 * ***************************************************************************
 * $Author$ $Id$ $DateTime$
 * ***************************************************************************/

package com.mindstix;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/** 
 * @author Milind Sonawane
 */

@RestController
@RequestMapping("/redis")
public class RedisController {
    
    @Autowired private IRedisRepo redis;
    
    @PostMapping
    public void postOperation(@RequestHeader HttpHeaders headers) {
        redis.save(headers.getFirst("key"), headers.getFirst("value"));
    }
    
    @DeleteMapping
    public void deleteOperation(@RequestHeader HttpHeaders headers) {
        redis.remove(headers.getFirst("key"));
    }
    
    @GetMapping
    public String getOperation(@RequestHeader HttpHeaders headers) {
        return redis.peek(headers.getFirst("key"));
    }
}