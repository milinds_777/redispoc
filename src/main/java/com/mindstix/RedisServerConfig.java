/* ***************************************************************************
 * Copyright 2018 VMware, Inc.  All rights reserved.
 * -- VMware Confidential
 * ***************************************************************************
 * $Author$ $Id$ $DateTime$
 * ***************************************************************************/

package com.mindstix;

import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisSentinelConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.StringUtils;
import org.springframework.cloud.Cloud;
import org.springframework.cloud.CloudFactory;
import org.springframework.cloud.service.common.RedisServiceInfo;

import redis.clients.jedis.JedisPoolConfig;

@Configuration
public class RedisServerConfig extends CachingConfigurerSupport {
    
    private static final String CACHE_NAME = "user-cache";

    @Value("${redis.sentinel.nodes}")
    private String sentinelNodes;

    @Value("${redis.sentinel.password}")
    private String redisPassword;

    @Value("${redis.sentinel.master.name}")
    private String redisMasterName;

    @Value("${redis.sentinel.port}")
    private Integer redisPort;

    @Value("${redis.server.cache.default.expiry.time}")
    private int expiryTime;

    @Value("${redis.pool.max.wait.seconds}")
    private int secondsToWait;

    @Value("${redis.pool.max.total}")
    private int maxTotal;

    @Value("${redis.pool.max.idle}")
    private int maxIdle;

    @Value("${redis.pool.min.idle}")
    private int minIdle;
    
    @Value("${redis.pcf.service.name}")
    private String redisServiceName;

    List<String> cacheNames = Arrays.asList("abcd");

    
    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyPlaceholderConfigurer() {
        
        return new PropertySourcesPlaceholderConfigurer();
    }

    /**
     * Function to create RedisConnectionFactory bean for sentinel nodes
     * 
     * @return                          {@link RedisConnectionFactory}
     */
    /*@Bean
    public RedisConnectionFactory connectionFactory() {

        JedisPoolConfig poolConfig = new JedisPoolConfig();
        poolConfig.setMaxTotal(maxTotal);
        poolConfig.setMaxIdle(maxIdle);
        poolConfig.setMinIdle(minIdle);
        poolConfig.setTestOnBorrow(true);
        poolConfig.setTestOnReturn(true);
        poolConfig.setTestWhileIdle(true);
        poolConfig.setMaxWaitMillis(secondsToWait);
        JedisConnectionFactory factory = new JedisConnectionFactory(sentinelConfig(), poolConfig);
        factory.setUsePool(true);
        factory.setPassword(redisPassword);
        factory.setPort(redisPort);
        return factory;
    }*/

    /**
     * Bean to create Redis Sential configuration 
     * 
     * @return                  {@link RedisSentinelConfiguration}
     */ 
    @Bean
    public RedisSentinelConfiguration sentinelConfig() {

        String[] nodes = StringUtils.commaDelimitedListToStringArray(sentinelNodes);
        final RedisSentinelConfiguration sentinelConfig = new RedisSentinelConfiguration();
        sentinelConfig.master(redisMasterName);
        for (String node : nodes) {
            sentinelConfig.sentinel(node, redisPort);
        }
        return sentinelConfig;
    }

    /**
     * Bean to create Redis template 
     * 
     * @return  RedisTemplate                   {@link RedisTemplate}
     */
    @Bean
    @Primary
    public RedisTemplate<String, String> redisTemplate() {
        
        RedisTemplate<String, String> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(connectionFactory());
        redisTemplate.expire(CACHE_NAME, 2, TimeUnit.MINUTES);
        return redisTemplate;
    }

    /**
     * This bean is used to manage the cache properties like expiry time
     * 
     * @param redisTemplate             Redis template for which properties has to set.
     * @return                          {@link CacheManager}
     */
    @Bean
    public CacheManager cacheManager(RedisTemplate<String, String> redisTemplate) {

        RedisCacheManager redisCacheManager = new RedisCacheManager(redisTemplate);
        redisCacheManager.setUsePrefix(true);
        redisCacheManager.setCacheNames(cacheNames);
        redisCacheManager.setTransactionAware(true);
        redisCacheManager.setLoadRemoteCachesOnStartup(true);
        redisCacheManager.afterPropertiesSet();
        return redisCacheManager;
    }

    @Bean
    @Override
    public KeyGenerator keyGenerator() {

        return (target, method, params) -> {
        
                StringJoiner sb = new StringJoiner("");
                sb.add(target.getClass().getName()).add(method.getName());
                sb.add(Arrays.stream(params).map(Object::toString).collect(Collectors.joining()));
                return sb;
        };
    }
    
    /**
     * Bean to create <code>RedisConnectionFactory</code> for PCF
     * @return
     */
    @Bean
    public RedisConnectionFactory connectionFactory() {
        CloudFactory cloudFactory = new CloudFactory();
        Cloud cloud = cloudFactory.getCloud();
        // Get service name from Environment
        RedisServiceInfo serviceInfo = (RedisServiceInfo) cloud.getServiceInfo(redisServiceName);
        String serviceID = serviceInfo.getId();
        return cloud.getServiceConnector(serviceID, RedisConnectionFactory.class, null);
    }
}