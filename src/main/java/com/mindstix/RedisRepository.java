/* ***************************************************************************
 * Copyright 2018 VMware, Inc.  All rights reserved.
 * -- VMware Confidential
 * ***************************************************************************
 * $Author$ $Id$ $DateTime$
 * ***************************************************************************/

package com.mindstix;

import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.batch.BatchProperties.Initializer;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

/** 
 * @author Milind Sonawane
 */
@Repository
public class RedisRepository implements IRedisRepo {
    
    private static final String CACHE_NAME = "user-cache";
    @Value("${redis.server.cache.default.expiry.time}") 
    private Long redisTTL;
    
    @Autowired
    RedisTemplate<String, String> redis;
    
    @PostConstruct
    public void initializer() {
        redis.expire(CACHE_NAME, redisTTL, TimeUnit.MINUTES);
    }
    
    @Override
    @Cacheable(value=CACHE_NAME)
    public void save(String key, String value) {
        redis.opsForHash().put(CACHE_NAME, key, value);
    }
    
    @Override
    @CacheEvict(value=CACHE_NAME)
    public void remove(String key) {
        redis.opsForHash().delete(CACHE_NAME, key);
    }
    
    @Override
    public String peek(String key) {
        return redis.opsForHash().get(CACHE_NAME, key).toString();
    }
}