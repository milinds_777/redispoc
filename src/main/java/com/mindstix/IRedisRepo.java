/* ***************************************************************************
 * Copyright 2018 VMware, Inc.  All rights reserved.
 * -- VMware Confidential
 * ***************************************************************************
 * $Author$ $Id$ $DateTime$
 * ***************************************************************************/

package com.mindstix;

public interface IRedisRepo {
    
    public void save(String key, String value);
    public void remove(String key);
    public String peek(String key);
}